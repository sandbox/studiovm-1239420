(function ($) {
	// ICI, votre code jQuery habituel
	$(document).ready(function() {
		// ICI, mettre votre code à vous.
		
		// Change l'attribut "target" des liens externes
		// de sorte qu'ils s'ouvrent dans une nouvelle fenêtre.
		$("a[href^='http']").attr('target', '_blank');
		
		// Ajoute une classe "external-link" aux liens externes.
		// Cette classe est définie dans le fichier modscript.css
		// présent dans ce module, et fait apparaître l'icône
		// lien externe à droite de chaque lien externe.
		$(".node a[href^='http']").addClass('external-link');
		
		// Plie et déplie le contenu des blocs quand on clique
		// sur leur titre.
		// Le sélecteur '.block h2' matche le titre de tous les blocs.
		$('.block h2').click(function() {
			// $(this) représente le titre qui vient d'être cliqué, soit
			// la balise <h2>. A partir de cette balise, on remonte d'un
			// niveau dans le DOM grâce à .parent() pour arriver au <div>
			// qui contient tout le bloc, et on cherche la balise qui
			// porte la classe .content, car c'est elle qu'on veut
			// masquer/afficher grâce à la commande slideToggle().
			$(this).parent().find('.content').slideToggle();
		});
		
		
		
		$(document).ready(function(){  
			  
			/* background resizable */     
			function redimensionnement(){  
			  
			    var image_width = $('imgfb').width();  
			    var image_height = $('imgfb').height();      
			      
			    var over = image_width / image_height;  
			    var under = image_height / image_width;  
			      
			    var body_width = $(window).width();  
			    var body_height = $(window).height();  
			      
			    if (body_width / body_height >= over) {  
			      $('imgfb').css({  
			        'width': body_width + 'px',  
			        'height': Math.ceil(under * body_width) + 'px',  
			        'left': '0px',  
			        'top': Math.abs((under * body_width) - body_height) / -2 + 'px'  
			      });  
			    }   
			      
			    else {  
			      $('imgfb').css({  
			        'width': Math.ceil(over * body_height) + 'px',  
			        'height': body_height + 'px',  
			        'top': '0px',  
			        'left': Math.abs((over * body_height) - body_width) / -2 + 'px'  
			      });  
			    }  
			}  
			          
			    redimensionnement(); //onload  
			      
			    $(window).resize(function(){  
			        redimensionnement();  
			    });  
			  
			}); 
		
		
	});
})(jQuery);